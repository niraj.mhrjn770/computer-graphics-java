package part_two;

/* Write a program in java to draw a line using BSA algorithm
         where an initial coordinate is (100, 300) and (500, 500). */
import java.awt.*;
import javax.swing.*;

public class QuesOne extends JFrame {
    QuesOne() {
        setTitle("Bresenham Line");
        setSize(800, 600);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void paint(Graphics g) {
        super.paint(g);
        int x1 = 100, y1 = 300, x2 = 500, y2 = 500;
        int dx = Math.abs(x2 - x1);
        int dy = Math.abs(y2 - y1);
        int p = 2 * dy - dx;
        int twoDy = 2 * dy;
        int twoDyMinusDx = 2 * (dy - dx);
        int x, y;
        if (x1 > x2) {
            x = x2;
            y = y2;
            x2 = x1;
            y2 = y1;
        } else {
            x = x1;
            y = y1;
        }
        g.fillOval(x, y, 5, 5);
        while (x < x2) {
            x++;
            if (p < 0) {
                p += twoDy;
            } else {
                y++;
                p += twoDyMinusDx;
            }
            g.fillOval(x, y, 5, 5);
        }
    }

    public static void main(String[] args) {
        new QuesOne();
    }
}
