package part_two;

/*Write a java program to display following object in given layout: */

import java.awt.*;
import javax.swing.*;

public class QuesTwo extends JFrame {

    QuesTwo() {
        super("QuesTwo");
        setSize(400, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        g.setColor(Color.BLACK);
        g.drawRoundRect(300, 300, 300, 100, 20, 20);

        g.setFont(new Font("Arial", Font.BOLD, 18));
        g.setColor(Color.RED);
        g.drawString("JAVA Programming in Graphics", 310, 350);

        g.setColor(Color.PINK);
        g.fillOval(800, 300, 300, 200);

        g.setColor(Color.RED);
        g.setFont(new Font("Arial", Font.ITALIC | Font.BOLD, 18));
        g.drawString("This is oval", 900, 400);

    }

    public static void main(String[] args) {
        new QuesTwo();
    }
}
