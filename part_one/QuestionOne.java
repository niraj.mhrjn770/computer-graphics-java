package part_one;

/*
 * 
 *Write a program in java to draw a line using DDA algorithm where x1 = 200, y1 = 500, x2 = 600 and y2 = 300.
 * 
 */
import java.awt.*;
import javax.swing.*;

public class QuestionOne extends JFrame {
    QuestionOne() {
        setTitle("DDA Line");
        setSize(800, 600);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void paint(Graphics g) {
        super.paint(g);
        int x1 = 200, y1 = 500, x2 = 600, y2 = 300;
        int dx = x2 - x1;
        int dy = y2 - y1;
        int steps = Math.max(Math.abs(dx), Math.abs(dy));
        float xIncrement = (float) dx / (float) steps;
        float yIncrement = (float) dy / (float) steps;
        float x = x1;
        float y = y1;
        g.fillOval((int) x, (int) y, 2, 2);
        for (int k = 0; k < steps; k++) {
            x += xIncrement;
            y += yIncrement;
            g.fillOval((int) x, (int) y, 2, 2);
        }
    }

    public static void main(String[] args) {
        new QuestionOne();
    }
}
