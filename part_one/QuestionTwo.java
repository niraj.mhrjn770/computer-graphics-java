package part_one;

/*
 * Write a program in java to display round rectangle with its level of roundness 20,20, 
 *      rectangle with “hello rectangle” inside it. Select your own coordinate position.
 */
import javax.swing.*;
import java.awt.*;

public class QuestionTwo extends JFrame {
    QuestionTwo() {
        setTitle("Round Rectangle");
        setSize(800, 600);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void paint(Graphics g) {
        super.paint(g);
        int x = 300, y = 200, width = 200, height = 100;
        int arcWidth = 20, arcHeight = 20;
        g.setColor(Color.BLACK);
        g.drawRoundRect(x, y, width, height, arcWidth, arcHeight);
        g.drawString("Hello Rectangle", x + 50, y + 50);
    }

    public static void main(String[] args) {
        new QuestionTwo();
    }
}
